import req from '@/libs/request'


export function login(data: any) {
    return req.post('/api/login', data)
}