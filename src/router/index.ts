import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'

const routes = [
  {
    path: '/home',
    name: 'home',
    component: () => import( '@/views/home.vue')
  },
  {
    path: '/',
    name: 'home',
    component: () => import( '@/views/home.vue')
  },
  {
    path: '/demo',
    name: 'demo',
    component: () => import( '@/views/demo.vue')
  },
  {
    path: '/login',
    name: 'login',
    component: () => import( '@/views/login.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
