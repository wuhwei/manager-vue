import jsCookie from 'js-cookie'
import { ElMessage } from 'element-plus'

const token = 'ticket.token'

const filter = (res: any) => {
    if (!res.success) {
        throw new Error(res.errMsg)
    }
    return res.data
}

class req {

    get = async (url: string, params?: Record<string, string>) => {
        url = params ? `${url}?${(new URLSearchParams(params)).toString()}` : url
        let res = await fetch(url, {
            headers: {
                'Authorization': jsCookie.get(token),
                'Content-Type': 'application/json',
            }
        }).then(resp => resp.json()).then(res => filter(res)).catch(e => console.log(e))
        return res
    }

    post = async (url: string, data: any, params?: Record<string, string>) => {
        url = params ? `${url}?${(new URLSearchParams(params)).toString()}` : url
        let res = await fetch(url, {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Authorization': jsCookie.get(token),
                'Content-Type': 'application/json',
            }
        }).then(resp => resp.json()).then(res => filter(res)).catch(e => {
            ElMessage.error(e.message)
            throw e
        })
        return res
    }

    put = async (url: string, data: any, params?: Record<string, string>) => {
        url = params ? `${url}?${(new URLSearchParams(params)).toString()}` : url
        let res = await fetch(url, {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Authorization': jsCookie.get(token),
                'Content-Type': 'application/json',
            }
        }).then(resp => resp.json()).then(res => filter(res)).catch(e => console.log(e))
        return res
    }

    delete = async (url: string, data: any, params?: Record<string, string>) => {
        url = params ? `${url}?${(new URLSearchParams(params)).toString()}` : url
        let res = await fetch(url, {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Authorization': jsCookie.get(token),
                'Content-Type': 'application/json',
            }
        }).then(resp => resp.json()).then(res => filter(res)).catch(e => console.log(e))
        return res
    }
}

export default new req();